public class CodilityTest {
	
	public int shoesCount(String s) {
		int packs = 0;
		int n = s.length();
		
		for (int i = 0; i < n - 1; i++) {
			char ch1 = s.charAt(i);
			char ch2 = s.charAt(i + 1);
			int r = 0;
			int l = 0;
			if (ch1 != ch2) {
				packs++;
				i++;
			} else {
				if (ch1 == 'R')
					r = 2;
				else
					l = 2;
				
				Boolean sideL = null;
				Boolean sideR = null;
				for (int j = i + 2; j < n - 2 ; j++) {
					char ch3 = s.charAt(j);
					if (ch3 == 'R') {
						r++;
						if (ch1 != ch3) {
							sideR = true;
						}
						if ((sideL != null) && (sideL == true))
							return packs + 1;
					} else {
						l++;
						if (ch1 != ch3) {
							sideL = true;
						}
						if ((sideR != null) && (sideR == true))
							return packs + 1;
					}
					if (r == l) {
						packs++;
						i = j;
						break;
					}
				}
			}
			
		}
		System.out.println(packs);
		return packs;
	}
}